# initech-webapp
initech-webapp

## Assumptions
* The Initech Website Supports only US phone numbers with or without country code
* Assume that the masked email/phone number value returned by the masking logic is saved in the database
* Assume the mask string for email id to be "*****" between the first and last letter of email id
