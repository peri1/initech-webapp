package com.initech.customer.pii;

import com.initech.customer.pii.constants.Constants;
import com.initech.customer.pii.exception.ValidationException;
import com.initech.customer.pii.util.MaskUtil;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MaskUtilTest {

	@Test
	public void testEmailMasking() throws Exception {
//		Input: "testemail@gmail.com"
//		Output: "t*****l@gmail.com"
//		Explanation: All names are converted to lowercase, and the letters between the
//		             first and last letter of the first name is replaced by 5 asterisks.
//		             Therefore, "testemail" -> "t*****l".
		String email = "testemail@gmail.com";
		String result = new MaskUtil().mask(email);
		Assert.assertEquals("t*****l@gmail.com", result);
	}

	@Test
	public void testEmailMasking_CamelCase() throws Exception{
//		Input: "TestEmail@gmail.com"
//		Output: "t*****l@gmail.com"
//		Explanation: There must be 5 asterisks between the first and last letter
//		             of the email "Tl". Therefore, "tl" -> "t*****l".
		String email = "TestEmail@gmail.com";
		String result = new MaskUtil().mask(email);

		Assert.assertEquals("t*****l@gmail.com", result);
	}

	@Test
	public void testPhoneNumberMasking() throws Exception{
//		Input: "1(470)258-0816"
//		Output: "470-***-0816"
//		Explanation: 10 digits in the phone number, which means all digits make up the local number.
		String phoneNumber = "(470)258-0816";
		String result = new MaskUtil().mask(phoneNumber);

		Assert.assertEquals("470-***-0816", result);
	}

	@Test
	public void testPhoneNumberMaskingWithCountryCodePrefix() throws Exception{
//		Input: "+1(470)258-0816"
//		Output: "+1470-***-0816"
//		Explanation: 12 digits, 2 digits for country code and 10 digits for local number.
		String phoneNumber = "+1(470)258-0816";
		String result = new MaskUtil().mask(phoneNumber);

		Assert.assertEquals("+1470-***-0816", result);
	}

	@Test
	public void testInvalidEmailOrPhoneNumberLength() {
//		Input: "test11111111111111111111111111111111111111111111111111111111111111email@gmail.com"
		String email = "test11111111111111111111111111111111111111111111111111111111111111email@gmail.com";
		Exception exception = assertThrows(ValidationException.class, () -> {
			new MaskUtil().mask(email);
		});
		String expectedMessage = Constants.INVALID_EMAIL_PHONE_NUMBER_LENGTH;
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void testEmptyEmailOrPhoneNumberLength() {

		Exception exception = assertThrows(ValidationException.class, () -> {
			new MaskUtil().mask(null);
		});
		String expectedMessage = Constants.NULL_OR_EMPTY_INPUT;
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));

		exception = assertThrows(ValidationException.class, () -> {
			new MaskUtil().mask("");
		});
		expectedMessage = Constants.NULL_OR_EMPTY_INPUT;
		actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}
}
