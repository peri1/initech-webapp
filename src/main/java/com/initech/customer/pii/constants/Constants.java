package com.initech.customer.pii.constants;

public class Constants {

    public static final String NULL_OR_EMPTY_INPUT = "Email or phone number is null or empty";
    public static final String INVALID_EMAIL_PHONE_NUMBER_LENGTH = "Email or phone number cannot be greater than " +
            "80 characters";

    public final static String AT_SYMBOL = "@";
    public final static String HYPHEN_SYMBOL = "-";
    public final static String ASTERISK_SYMBOL = "*";
    public final static String PLUS_SYMBOL = "+";
    public final static String MASK_STRING = "*****";
}
