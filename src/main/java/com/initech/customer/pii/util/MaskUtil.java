package com.initech.customer.pii.util;

import com.initech.customer.pii.constants.Constants;
import com.initech.customer.pii.exception.ValidationException;

import java.util.Objects;

/**
 * This utility class exposes a mask() method which masks an input email id or phone number
 * Assumptions:
 * * Supports only US phone numbers with or without +1 as prefix
 * * The masked email/phone number value returned by the below masking logic is saved in the database
 */
public class MaskUtil {

    /**
     * This method validates an input email or phone number and returns a masked value
     *
     * @param inputString
     * @return
     */
    public String mask(String inputString)
    {
        if(Objects.isNull(inputString) || inputString.length() == 0)
            throw new ValidationException(Constants.NULL_OR_EMPTY_INPUT);
        else if(inputString.length() > 80)
            throw new ValidationException(Constants.INVALID_EMAIL_PHONE_NUMBER_LENGTH);
        else if(inputString.indexOf(Constants.AT_SYMBOL) > 0)
            return maskEmail(inputString);
        else
            return maskPhoneNumber(inputString);
    }

    /**
     * This method masks an input phone number and returns the masked value
     *
     * @param inputPhoneNumber
     * @return
     */
    private String maskPhoneNumber(String inputPhoneNumber)
    {
        StringBuilder builder = new StringBuilder();
        int length = inputPhoneNumber.length();
            int count = 0;
            for (int index = length - 1; index >= 0; index--)
            {
                char ch = inputPhoneNumber.charAt(index);
                if (ch >= '0' && ch <= '9') {
                    if (count < 4 || count > 7) {
                        builder.insert(0, ch);

                    } else if (count == 7) {
                        builder.insert(0, Constants.HYPHEN_SYMBOL);
                        builder.insert(0, ch);
                    } else {
                        if (count == 4) {
                            builder.insert(0, Constants.HYPHEN_SYMBOL);
                        }
                        builder.insert(0, Constants.ASTERISK_SYMBOL);
                    }
                    count++;
                }
            }
            if (count > 10) {
                builder.insert(0, Constants.PLUS_SYMBOL);
            }
        return builder.toString();
    }

    /**
     *This method masks an input email and returns the masked value
     * @param inputEmail
     * @return
     */
    String maskEmail(String inputEmail)
    {
        // define :
        // inputString.length ≥ 80
        // All email addresses are guaranteed to be valid and in the format of "name1@name2.name3".

        // all names must be converted to lowercase and
        // all letters between the first and last letter of the first name must be replaced by 5 asterisks '*'

            String[] emailTokens = inputEmail.split(Constants.AT_SYMBOL);
            String firstName = emailTokens[0];
            StringBuilder builder = new StringBuilder();
            builder.append(firstName.charAt(0));
            builder.append(Constants.MASK_STRING);
            builder.append(firstName.charAt(firstName.length() - 1));
            builder.append(Constants.AT_SYMBOL).append(emailTokens[1]);

            return builder.toString().toLowerCase();
    }

}