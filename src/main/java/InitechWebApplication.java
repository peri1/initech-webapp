import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InitechWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(InitechWebApplication.class, args);
	}
}
